<!DOCTYPE html>
<html>
<head>
	<title>Registration</title>
    <link rel="icon" type="image/ico" href="images/register.png" />

     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

</head>
<body class="m-5">
<div class="container">
<form action="registeruser.php" method="post">
    

    <hr style="border-width:5px;" class="alert-dark" />
    <div class="row" style="background-image: url(/images/back.jpg); background-position: center;">
            <div class="col-lg-12"><h1 class="text-primary" align="center">Registration</h1></div>
      </div>

      <hr style="border-width:5px;" class="alert-dark" />

    	<div class="row mt-5 mb-5 alert-info">
    		<div class="col-lg-4 mt-5 mb-5">
          <div class="form-group">
                        <label for="usernm">User Name :</label>
                        <input type="text" class="form-control" id="unm" name="unm" placeholder="Enter name" required>
                </div>

                <br>

                 <div class="form-group">
                        <label for="age">Age :</label>
                        <input type="text" class="form-control" id="age" name="age" placeholder="Enter age" required>
                  </div>

                  <br>

                   <div class="form-group">
                    <label for="qualification">Qualification :</label>
                    <input type="text" class="form-control" id="qualification" name="qualification" placeholder="Enter Qualification" required>
                    </div>

                    <br>

                  <div class="form-group">
                        <label for="userid">User ID :</label>
                        <input type="text" class="form-control" id="uid" name="uid" placeholder="Enter id" required>
                  </div>

        </div>

    		<div class="col-lg-4 mt-5">
        
        <div class="form-group">
                        <label for="dob">Date of Birth :</label>
                        <input type="text" class="form-control" id="dob" name="dob" placeholder="Enter date" required>
                </div>


                <br>

                   <div class="form-group">
                        <label for="number">Mobile Number :</label>
                        <input type="text" class="form-control" id="num" name="num" placeholder="Enter number" maxlength="10" minlength="10" required>
                </div>

                <br>

                 <div class="form-group">
                        <label for="address">Address :</label>
                        <input type="text" class="form-control" id="addr" name="addr" placeholder="Enter address" required>
                </div>

                <br>


                   <div class="form-group">
                    <label for="password">Password :</label>
                    <input type="password" class="form-control" id="pw" name="pw" placeholder="Enter Password" required>
                    </div>
                 </div>

    		<div class="col-lg-4 mt-5">

         <div class="form-group">
                        <label for="gender">Gender :</label><br><h4>
                        <label class="mr-5"><input type="radio" name="optradio" id="optradiof" checked value="Female">Female</label>
                        <label><input type="radio" name="optradio" id="optradiom" value="Male">Male</label></h4>
                </div>

                <br>

                 <div class="form-group">
                        <label for="email">Email :</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
                  </div>

                  <br>

          <div class="form-group">
                        <label for="state">State :</label>
                        <input type="text" class="form-control" id="state" name="state" placeholder="Enter state" required>
                  </div>

                  <br>

                   <div class="form-group">
                    <label for="repassword">Retype Password :</label>
                    <input type="password" class="form-control" id="rpw" name="rpw" placeholder="Re-enter Password" required>
                    </div>
                  <br>

                     <div align="right">
                          <button type="submit" class="btn btn-dark" style="font-family: 'Comic Sans MS'">Register</button>
                          <a href="success.php" class="btn btn-dark">Back</a>
                 </div>
                 <br>  
        </div>
    	</div>
    </div>
</form>

<script type="text/javascript">
 var password = document.getElementById("pw"),retypepw = document.getElementById("rpw");

 function validatePassword()
 {
       if(password.value != retypepw.value) 
       {
         retypepw.setCustomValidity("Passwords Don't Match");
     }  
     else 
     {
         retypepw.setCustomValidity('');
       }
 }

 password.onchange = validatePassword;
 retypepw.onkeyup = validatePassword;
 </script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<hr style="border-width:5px;" class="alert-dark" /> 
</body>

<script type = "text/javascript" >
   function preventBack()
   {
   	window.history.forward();
   }
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>

</html>
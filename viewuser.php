<!DOCTYPE html>
<html>
<head>
	<title>View</title>
	<link rel="icon" type="image/ico" href="images/view.png" />
</head>

<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

<body class="m-5">

<?php

$id = $_GET['id'];

$con = new mysqli("localhost", "root", "", "userdb");

    $result = mysqli_query($con, "Select * from users WHERE userid= '{$id}'");

    $rows = mysqli_fetch_array($result);

    $unm = $rows['name'];
    $gender = $rows['gender'];
    $dob = $rows['dob'];
    $age = $rows['age'];
    $num = $rows['mobile'];
    $email = $rows['email'];
    $qualification = $rows['qualification'];
    $addr = $rows['address'];
    $state = $rows['state'];
    $uid = $rows['userid'];

?>

    <form action="success.php" method="post">
    <div class="container">

     <hr style="border-width:5px;" class="alert-dark" />
    	<div class="row" style="background-image: url(/images/back.jpg); background-position: center;">
            <div class="col-lg-12"><h1 class="text-primary" align="center">User Details</h1></div>
        </div>
        <hr style="border-width:5px;" class="alert-dark" />

        <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4 mt-5">
        	<table cellpadding="4">
        	<tr>
        		<th>Name : </th>
        		<td><?php echo $unm ?></td>
        	</tr>

            <tr>
                <th>Gender : </th>
                <td><?php echo $gender ?></td>
            </tr>

            <tr>
                <th>Date of Birth : </th>
                <td><?php echo $dob ?></td>
            </tr>

            <tr>
                <th>Age : </th>
                <td><?php echo $age ?></td>
            </tr>

        	<tr>
        		<th>Mobile Number : </th>
        		<td><?php echo $num ?></td>
        	</tr>

            <tr>
                <th>Email Address : </th>
                <td><?php echo $email ?></td>
            </tr>

            <tr>
                <th>Qualification : </th>
                <td><?php echo $qualification ?></td>
            </tr>

        	<tr>
        		<th>Address : </th>
        		<td><?php echo $addr ?></td>
        	</tr>

            <tr>
                <th>State : </th>
                <td><?php echo $state ?></td>
            </tr>

        	<tr>
        		<th>User id : </th>
        		<td><?php echo $uid ?></td>
        	</tr>

        	</table>

        	<br>
        	
 			<button class="btn btn-dark pull-right">Back</button>

        </div>
        </div>
        <hr style="border-width:5px;" class="alert-dark" />
    </div>
    </form>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

</body>
</html>
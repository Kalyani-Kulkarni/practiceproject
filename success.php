<html>
<head>
<title>Success</title>
<link rel="icon" type="image/ico" href="images/loginsuccess.png" />
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">


</head>
<body class="m-5">
<div class="container">
    <hr style="border-width:5px;" class="alert-dark" />
    <div class="row" style="background-image: url(/images/back.jpg); background-position: center;">
            <div class="col-lg-12"><h1 class="text-primary" align="center">Users</h1></div>
      </div>

      <hr style="border-width:5px;" class="alert-dark" />
<div class="row">
<div class="col-lg-6 text-dark">
<h4>
	 <?php

    session_start();

    if($_SESSION==NULL)
    {
      echo "Unauthorised Access ";
      echo "<a href='login.html'>Login First</a>";
    }else
    {
      echo "Welcome, ".$_SESSION['id'];


	?></h4>
  </div>
  <div class="col-lg-6">
     <a href="logout.php" class="btn btn-dark pull-right"> Logout</a>
     <a href="register.php" class="btn btn-dark pull-right mr-2"> Add New User</a>
  </div>
  </div>

	<br><br>

	  <table class="table table-hover table-bordered">

      <thead class="alert-info" style="text-align: center;">

           <tr  style="text-align: center;">

                     <th>User Name</th>
                     <th>Gender</th>
                     <th>Date of Birth</th>
                     <th>Age</th>
                     <th>Mobile Number</th>
                     <th>Email Address</th>
                     <th>Qualification</th>
                     <th>Address</th>
                     <th>State</th>
                     <th>User ID</th>
                     <th width="15%">Action</th>
           </tr>

      </thead>

<?php

	$con = new mysqli("localhost", "root", "", "userdb");

	$result = mysqli_query($con, "Select * from users");

     while ($rows = mysqli_fetch_array($result)){
     	$id = $rows['userid'];

           echo "<tr>";
               echo "<td>".$rows['name']."</td>";
               echo "<td>".$rows['gender']."</td>";
               echo "<td>".$rows['dob']."</td>";
               echo "<td>".$rows['age']."</td>";
               echo "<td>".$rows['mobile']."</td>";
               echo "<td>".$rows['email']."</td>";
               echo "<td>".$rows['qualification']."</td>";
               echo "<td>".$rows['address']."</td>";
               echo "<td>".$rows['state']."</td>";
               echo "<td>".$rows['userid']."</td>";
               echo '<td  style="text-align: center;">
               <a class="btn btn-small btn-success mr-1" title="Edit" href="updateuser.php?id='.$id.'" name="edit"><i class="glyphicon glyphicon-pencil"></i></a><a class="btn btn-small btn-success mr-1" title="View" href="viewuser.php?id='.$id.'" name="view"><i class="glyphicon glyphicon-search"></i></a><a class="btn btn-small btn-success" title="Delete" href="deleteuser.php?id='.$id.'" name="delete"><i class="fa fa-trash"></i></a></td>';

           echo "</tr>";
     }}
    ?>
    </table>

    <br>
    </div>
    <hr style="border-width:5px;" class="alert-dark" />

</body>

<script type = "text/javascript" >
   function preventBack()
   {
   	window.history.forward();
   }
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>

</html>
<!DOCTYPE html>
<html>
<head>
    <title>Updation</title>
    
    <link rel="icon" type="image/ico" href="images/update.png" />

     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

</head>
<body class="m-5">

<form action="update.php" method="post">
    <div class="container">
        <hr style="border-width:5px;" class="alert-dark" />
    <div class="row" style="background-image: url(/images/back.jpg); background-position: center;">
            <div class="col-lg-12"><h1 class="text-primary" align="center">Updation</h1></div>
      </div>
      <hr style="border-width:5px;" class="alert-dark" />

      <?php

    $id = $_GET['id'];

    $con = new mysqli("localhost", "root", "", "userdb");

    $result = mysqli_query($con, "Select * from users WHERE userid= '{$id}'");

    $rows = mysqli_fetch_array($result);

    $unm = $rows['name'];
    $gender=$rows['gender'];
    $dob=$rows['dob'];
    $age=$rows['age'];
    $num = $rows['mobile'];
    $email=$rows['email'];
    $qualification=$rows['qualification'];
    $addr = $rows['address'];
    $state=$rows['state'];
    $uid = $rows['userid'];
    $pw = $rows['password'];
    ?>

            <div class="row mt-5 mb-5 alert-info">
            <div class="col-lg-4 mt-5 mb-5">
          <div class="form-group">
                        <label for="usernm">User Name :</label>
                        <input type="text" class="form-control" id="unm" name="unm" placeholder="Enter name" value="<?= $unm ?>" required>
                </div>

                <br>

                 <div class="form-group">
                        <label for="age">Age :</label>
                        <input type="text" class="form-control" id="age" name="age" placeholder="Enter age" value="<?= $age ?>" required>
                  </div>

                  <br>

                   <div class="form-group">
                    <label for="qualification">Qualification :</label>
                    <input type="text" class="form-control" id="qualification" name="qualification" placeholder="Enter Qualification" value="<?= $qualification ?>" required>
                    </div>

                    <br>

                  <div class="form-group">
                        <label for="userid">User ID :</label>
                        <input type="text" class="form-control" id="uid" name="uid" placeholder="Enter id" value="<?= $uid ?>" required>
                  </div>

        </div>

            <div class="col-lg-4 mt-5">

        <div class="form-group">
                        <label for="dob">Date of Birth :</label>
                        <input type="text" class="form-control" id="dob" name="dob" placeholder="Enter date" value="<?= $dob ?>" required>
                </div>

                <br>

                   <div class="form-group">
                        <label for="number">Mobile Number :</label>
                        <input type="text" class="form-control" id="num" name="num" placeholder="Enter number" maxlength="10" minlength="10" value="<?= $num ?>" required>
                </div>

                <br>

                 <div class="form-group">
                        <label for="address">Address :</label>
                        <input type="text" class="form-control" id="addr" name="addr" placeholder="Enter address" value="<?= $addr ?>" required>
                </div>

                <br>


                   <div class="form-group">
                    <label for="password">Password :</label>
                    <input type="password" class="form-control" id="pw" name="pw" placeholder="Enter Password" value="<?= $pw ?>" required>
                    </div>
                 </div>

            <div class="col-lg-4 mt-5">

             <div class="form-group">
                        <label for="gender">Gender :</label><br><h4>
                      <?php  
                      if($gender=="Female")
                        {
                        ?>
                            <label class="mr-5"><input type="radio" name="optradio" id="optradiof" value="Female" checked>Female</label>

                            <label><input type="radio" name="optradio" id="optradiom" value="Male">Male</label>
                       <?php }
                        else
                        {
                            ?>
                            <label class="mr-5"><input type="radio" name="optradio" id="optradiof" value="Female">Female</label>
                            <label><input type="radio" name="optradio" id="optradiom" value="Male" checked>Male</label><?php } ?>
                        </h4>
                </div>

                <br>

                 <div class="form-group">
                        <label for="email">Email :</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" value="<?= $email ?>" required>
                  </div>

                  <br>

          <div class="form-group">
                        <label for="state">State :</label>
                        <input type="text" class="form-control" id="state" name="state" placeholder="Enter state" value="<?= $state ?>" required>
                  </div>

                  <br>
                  <br>
                  <br>

                     <div align="right">
                          <button type="submit" class="btn btn-dark" style="font-family: 'Comic Sans MS'">Update</button>
                          <a href="success.php" class="btn btn-dark">Back</a>
                 </div>
                 <br>  
        </div>
        </div>
    </div>
</form>
<hr style="border-width:5px;" class="alert-dark" /> 
</body>
</html>